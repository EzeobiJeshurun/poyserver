const mongoose = require('mongoose');
//_id: mongoose.Schema.Types.ObjectId

const usersSchema = mongoose.Schema({
    name: String,
    gender: String,
    email: String,
    location: String,
    password: String,
    timestamp: String,
});

module.exports = mongoose.model('Users', usersSchema);