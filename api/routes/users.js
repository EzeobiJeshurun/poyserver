const express = require('express');
const router = express.Router();
const Users = require('../models/users');
const dayjs = require('dayjs');

router.post('/register/:userId', (req, res, next) => {
    //does nothing with userId
    //password should be hashed, but not needed as this is a test
    const data =req.body;
    const newTimestamp = dayjs().format('YYYY-MM-DD');
    const newUser = new Users({
          name: data.name,
          gender: data.gender,
          email: data.email,
          location: data.location,
          password: data.password,
          timestamp: newTimestamp,
     });
    Users.find({email: data.email}).exec().then((result) => {
        if(result.length === 0) {
           newUser.save().then((savedData) => {
             res.status(200).json({
              data: savedData,
              
             });
            }).catch((error) => {
               res.status(400).json({
             error: "Something went wrong please try again"
           });  
          });  
      } else {
          res.status(400).json({
             error: "Email already in use",

           });  
      }
    }).catch((error)=> {
       res.status(400).json({
           error,
         });
    })
   
    
});


router.post('/login/:userId', (req, res, next) => {
    //does nothing with userId
    const data =req.body;

    
    Users.find({email: data.email, password: data.password}).exec().then((result) => {
      
            if(result.length > 0) {
                res.status(200).json({
                   data: {
                     email: result[0].email,
                     name: result[0].name,
                     location: result[0].location,
                     timestamp: result[0].timestamp,
                     id: result[0]._id,
                     gender: result[0].gender,
                   }
                 });
            } else {
                res.status(400).json({
                   error: "please Enter the correct details"
                });
            }
         }).catch((error) => {
         res.status(400).json({
           error: "please Enter the correct details"
         });
     });
   
    
});



module.exports = router;