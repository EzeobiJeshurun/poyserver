const express = require('express');
const app = express();
const cors = require('cors');
const morgan = require('morgan');
const bodyPaser = require('body-parser');
const mongoose = require('mongoose');

const usersRoutes = require('./api/routes/users');

//
const uri = 'mongodb+srv://jesh:t0RPnqTKhCaJunV9@node-rest-connect.lnbmn.mongodb.net/bookings?retryWrites=true&w=majority';
//if you want to test it locally, you must have mongodb running locally
//then use this as uri
//const uri = 'mongodb://127.0.0.1:27017/power-of-you-test-db'

mongoose.connect( uri , { useNewUrlParser: true,  useUnifiedTopology: true });

app.use(cors());
//display request status in console during development
app.use(morgan('dev'));
app.use(bodyPaser.urlencoded({ extended: false }));
app.use(bodyPaser.json());
app.use('/user', usersRoutes);

app.use('/',(req, res, next) => {
  res.send('welcome to Power of You');
      
});
//bellow catches all routes that does not match in our middleware

app.use((req, res, next) => {
    const error = new Error('request not found');
    error.status = 404;
    next(error);
});

//when an error occurs eg as a result of database failure and it is handled by the middleware above
app.use((error,req, res, next) => {
  res.status(error.status || 500).json({
      error: {
          message: error.message
        },
  });
});

module.exports = app;